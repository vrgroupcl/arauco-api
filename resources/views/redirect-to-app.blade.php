<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Arauco</title>
    <link rel="stylesheet" href="css/app.css">

</head>
<body class="oauth-redirect">
    <div class="wrapper">
        <a class="open-app" href="{{ $link }}">Continuar inicio de sesión <i class="fas fa-arrow-right"></i></a>
    </div>
    {{--<a href="">Instalar aplicación</a>--}}

    <script>
        window.location = '{{ $link }}';
        setTimeout(function() {
            document.getElementsByClassName('open-app')[0].style.opacity = 1
        }, 2000)
    </script>
</body>
</html>
