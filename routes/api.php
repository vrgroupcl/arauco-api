<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/me', function (Request $request) {
    return auth('api')->user();
});

Route::post('login', 'API\LoginController@login')->middleware('jwt_arauco');

Route::group(['middleware' => 'auth:api'], function () {
    Route::any('upload-audio', 'AudioUploadController@upload');
    Route::post('/issues', 'API\\IssueAPIController@store');
    Route::get('/issues', 'API\\IssueAPIController@index');
});
