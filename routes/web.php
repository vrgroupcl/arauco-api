<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//})->name('login');

use App\Issue;
use App\Jobs\CreateIssueJob;

Route::get('oauth', 'OAuthController@index');

Route::get('/test', function() {
    dispatch(new CreateIssueJob(Issue::find(21)));
});
//Route::get('/test', function () {
//    $client = \App\ServiceAnywhere\Client::getInstance();
//    $response = $client->getRequestById('241996')->execute();
//    dd($response);
////    $response = $client->createIncident('sap', 'vsagredo@arauco.cl', '<p>Descripción</p>')->execute();
////    if ($response->entity_result_list[0]->completion_status === "OK") {
////        $ticketId = $response->entity_result_list[0]->entity->properties->Id;
////    }
////    dd($ticketId, $response);
//});
