<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIssuesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('issues', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('external_id')->unsigned()->nullable();
            $table->string('location')->nullable();
            $table->string('category')->nullable();
            $table->string('status')->nullable();
            $table->text('message')->nullable();
            $table->string('original_audio')->nullable();
            $table->string('transformed_audio')->nullable();
            $table->text('transcripted_text')->nullable();
            $table->text('transcripted_confidence')->nullable();
            $table->boolean('ready_to_sync')->default(false);
            $table->timestamp('synced_at')->nullable();
            $table->unsignedInteger('user_id');
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('issues');
    }
}
