@setup
// Configure
$site_name = 'arauco';
$ssh_user = 'forge';
$server_ip = '167.99.150.146';
$server_base_path = '/home/forge/arauco.digitalpartner.cl';
$sync_folder =  '/storage/app/';

$env = [
'host'      => 'DB_HOST',
'name'      => 'DB_DATABASE',
'username'  => 'DB_USERNAME',
'password'  => 'DB_PASSWORD',
];

$ssh_connection = $ssh_user.'@'.$server_ip;
@endsetup

@servers(['server' => [$ssh_connection], 'local' => '127.0.0.1'])

@task('install', ['on' => 'local'])
if [ -f composer.json ]; then
echo "<info>Installing composer dependencies...</info>"
composer install --prefer-dist
fi

if [ -f package.json ]; then
echo "<info>Installing NPM dependencies...</info>"
npm install
fi

if [ -f webpack.mix.js ]; then
echo "<info>Compiling assets...</info>"
npm run dev
fi

if [ ! -f .env ]; then
cp .env.example .env
echo "<info>You should edit your new .env file</info>"
fi

@endtask

@story('pull')
remote:db:export
local:db:download_remote_export
local:db:import
remote:db:delete_dump
local:db:delete_dump
pull:uploads
@endstory

@story('push')
confirm
local:db:export
local:db:upload_local_export
remote:db:extract_dump
remote:db:import
remote:db:delete_dump
local:db:delete_dump
push:uploads
@endstory

@story('pull:database')
remote:db:export
local:db:download_remote_export
local:db:import
remote:db:delete_dump
local:db:delete_dump
@endstory

@story('push:database')
confirm
local:db:export
local:db:upload_local_export
remote:db:extract_dump
remote:db:import
remote:db:delete_dump
local:db:delete_dump
@endstory

@task('pull:uploads', ['on' => 'local'])
echo "Downloading remote files... "
rsync -avzP --exclude=bfi_thumb --exclude=cache {{ $ssh_connection }}:{{ $server_base_path }}{{ $sync_folder }} .{{ $sync_folder }}
echo "<info>Files successfully downloaded</info>"
@endtask

@task('push:uploads', ['on' => 'local'])
echo "Uploading local files... "
if [ -d .{{ $sync_folder }} ]; then
    rsync -avzP --exclude=bfi_thumb --exclude=cache  .{{ $sync_folder }} {{ $ssh_connection }}:{{ $server_base_path }}{{ $sync_folder }}
else
    echo "<error>There is not folder {{ $sync_folder }}</error>"
fi
echo "<info>Files successfully uploaded</info>"
@endtask

@task('env:remote', ['on' => 'server'])
cd {{ $server_base_path }}
echo "<info>Remote Environment Variables</info>"
cat {{ $server_base_path }}/.env
@endtask
@task('env:local', ['on' => 'local'])
echo "<info>Local Environment Variables</info>"
cat .env
@endtask

@story('env')
env:local
env:remote
@endstory

@task('remote:db:export', ['on' => 'server'])
cd {{ $server_base_path }}

if [ ! -f .env ]; then
echo "<error>There is no .env file to load database credentials.</error>"
exit 1
fi
source .env
if [ -z "${{ $env['password'] }}" ]; then
mysqldump -h ${{ $env['host'] }} -u ${{ $env['username'] }} -c -e --default-character-set=utf8 --single-transaction --skip-set-charset --add-drop-database ${{ $env['name'] }} | gzip -c > {{ $site_name }}.sql.gz
else
mysqldump -h ${{ $env['host'] }} -u ${{ $env['username'] }} -p${{ $env['password'] }} -c -e --default-character-set=utf8 --single-transaction --skip-set-charset --add-drop-database ${{ $env['name'] }} | gzip -c > {{ $site_name }}.sql.gz
fi
echo "Remote database exported"
@endtask

@task('local:db:export', ['on' => 'local'])
if [ ! -f .env ]; then
echo "<error>There is no .env file to load database credentials.</error>"
exit 1
fi
source .env
if [ -z "${{ $env['password'] }}" ]; then
mysqldump -h ${{ $env['host'] }} -u ${{ $env['username'] }} -c -e --default-character-set=utf8 --single-transaction --add-drop-database ${{ $env['name'] }} | gzip -c > {{ $site_name }}.sql.gz
else
mysqldump -h ${{ $env['host'] }} -u ${{ $env['username'] }} -p${{ $env['password'] }} -c -e --default-character-set=utf8 --single-transaction --add-drop-database ${{ $env['name'] }} | gzip -c > {{ $site_name }}.sql.gz
fi
echo "Local database exported"
@endtask

@task('local:db:download_remote_export', ['on' => 'local'])
if [ -f {{ $site_name }}.sql ]; then
echo "File {{ $site_name }}.sql deleted"
rm {{ $site_name }}.sql
fi

if [ -f {{ $site_name }}.sql.gz ]; then
echo "File {{ $site_name }}.sql.gz deleted"
rm {{ $site_name }}.sql.gz
fi

echo "Downloading database backup from server..."
rsync -avzP {{ $ssh_connection }}:{{ $server_base_path }}/{{ $site_name }}.sql.gz ./
echo "Database dump downloaded";
gzip -d {{ $site_name }}.sql.gz
echo "Database dump extracted"
@endtask


@task('local:db:upload_local_export', ['on' => 'local'])
echo "Uploading database backup from server..."
rsync -avzP ./{{ $site_name }}.sql.gz {{ $ssh_connection }}:{{ $server_base_path }}/
echo "<info>Local database dump uploaded</info>"
@endtask

@task('remote:db:extract_dump', ['on' => 'server'])
cd {{ $server_base_path }}
if [ -f {{ $site_name }}.sql ]; then
echo "File {{ $site_name }}.sql deleted"
rm {{ $site_name }}.sql
fi

gzip -d {{ $site_name }}.sql.gz
echo "Remote database dump extracted"
@endtask

@task('remote:db:import', ['on' => 'server'])
cd {{ $server_base_path }}
echo "Importing dump in remote database..."
source .env
if [ -z "${{ $env['password'] }}" ]; then
echo "Importing without mysql password"
mysql -h ${{ $env['host'] }} -u ${{ $env['username'] }} ${{ $env['name'] }} < {{ $site_name }}.sql --binary-mode
else
mysql -h ${{ $env['host'] }} -u ${{ $env['username'] }} -p${{ $env['password'] }} ${{ $env['name'] }} < {{ $site_name }}.sql --binary-mode
fi
echo "<info>Remote database updated</info>"
@endtask

@task('local:db:import', ['on' => 'local'])
echo "Importing dump in local database..."
source .env
if [ -z "${{ $env['password'] }}" ]; then
echo "Importing without mysql password"
mysql -h ${{ $env['host'] }} -u ${{ $env['username'] }} ${{ $env['name'] }} < {{ $site_name }}.sql --binary-mode
else
mysql -h ${{ $env['host'] }} -u ${{ $env['username'] }} -p${{ $env['password'] }} ${{ $env['name'] }} < {{ $site_name }}.sql --binary-mode
fi
echo "<info>Local database updated</info>"
@endtask

@task('remote:db:delete_dump', ['on' => 'server'])
cd {{ $server_base_path }}
if [ -f {{ $site_name }}.sql.gz ]; then
rm {{ $site_name }}.sql.gz
fi
if [ -f {{ $site_name }}.sql ]; then
rm {{ $site_name }}.sql
fi
echo "Database dump deleted"
@endtask


@task('local:db:delete_dump', ['on' => 'local'])
if [ -f {{ $site_name }}.sql.gz ]; then
rm {{ $site_name }}.sql.gz
fi
if [ -f {{ $site_name }}.sql ]; then
rm {{ $site_name }}.sql
fi
echo "Database dump deleted from local"
@endtask

@task('confirm', ['on' => 'local', 'confirm' => true])
echo "<info>Granted. Running under your own risk</info>"
@endtask

