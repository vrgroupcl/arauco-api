<?php

namespace App\Http\Controllers;

use GuzzleHttp\Client;
use Illuminate\Http\Request;

class OAuthController extends Controller
{
    public function index(Request $request)
    {
        if ($code = $request->get('code')) {
            return $this->code($code);
        }

        return response()->json(['error' => 'CODE_UNKNOWN']);
    }

    public function code($code)
    {
        if (config('adfs.is_proxy_adfs')) {
            return redirect()->to(config('adfs.proxy_redirect_url') . '?code=' . $code);
        }
        $client = new Client([
            // Base URI is used with relative requests
            'base_uri' => config('adfs.url'),
            // You can set any number of default request options.
            'timeout'  => 15,
        ]);

        try {
            $response = $client->post('/adfs/oauth2/token', [
                'form_params' => [
                    'grant_type' => 'authorization_code',
                    'client_id' => config('adfs.client_id'),
                    'redirect_uri' =>  config('adfs.redirect_uri'),
                    'code' => $code
                ]
            ]);
            $body = $response->getBody();
            $body = json_decode($body, true);
            return view('redirect-to-app', ['link' => 'arauco-poc://login?token=' . $body['access_token']]);
        } catch (\Exception $e) {
            \Log::error($e);
            return view('redirect-to-app', ['link' => 'arauco-poc://login?token_error=1']);
        }
    }
}
