<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LoginController extends Controller
{
    public function login()
    {
        $user = auth()->user();
        return [
            'user' => $user,
            'token' => [
                'access_token' => auth('api')->tokenById($user->id),
                'token_type' => 'bearer',
                'expires_in' => auth('api')->factory()->getTTL() * 60
            ]
        ];
    }
}
