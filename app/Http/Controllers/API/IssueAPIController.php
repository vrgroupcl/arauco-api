<?php

namespace App\Http\Controllers\API;

use App\Issue;
use App\Jobs\CreateIssueJob;
use Google\Cloud\Speech\SpeechClient;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class IssueAPIController extends Controller
{
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $issues = auth()->user()->issues()->notIncomplete()->orderBy('created_at', 'DESC')->paginate(25);
        return response()->json($issues);
    }

    /**
     * @param Request $request
     * @param SpeechClient $speechClient
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $request->validate([
            'message' => 'nullable|string|max:191',
            'category' => 'nullable|string',
            // 'location' => 'nullable|string',
        ]);

        \Log::debug('Issue received');
        \Log::debug(print_r($request->all(), true));

        $newIssue = new Issue();
        $newIssue->message = $request->get('message');
        $newIssue->location = $request->get('location', 'Lugar del incidente');
        $newIssue->category = $request->get('category');
        $newIssue->user_id = auth()->user()->id;
        $newIssue->status = Issue::STATUS_PROCESSING;
        $newIssue->has_audio = $request->get('hasAudio', true);
        if (!$newIssue->has_audio) {
            $newIssue->ready_to_sync = true;
        }

        $newIssue->save();
        $newIssue->id;

        //If the issue has no audio, then start the creation process on SAW directly, else wait for the audio in another request.
        if (!$request->get('hasAudio', true)) {
            \Log::debug('Issue without audio. Creating on SAW directly');
            CreateIssueJob::dispatch($newIssue);
        }

        return response()->json(['success' => true, 'data' => $newIssue]);
    }
}
