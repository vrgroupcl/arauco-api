<?php

namespace App\Http\Controllers;

use App\Issue;
use App\Jobs\AttachIssueAudioJob;
use App\Jobs\CreateIssueJob;
use App\Jobs\TranscriptAudioJob;
use App\Jobs\TransformIssueAudioToFlac;
use Illuminate\Http\Request;

class AudioUploadController extends Controller
{
    public function upload(Request $request)
    {
        $this->validate($request, [
            'file' => 'required|file',
            'issue_id' => 'required'
        ]);
        $issue = Issue::findOrFail($request->get('issue_id'));
        if ($issue->user_id !== auth()->user()->id) {
            return response()->json(['error' => 'NOT_AUTHORIZED_ISSUE_AUTHOR'], 401);
        }

        $path = $request->file('file')->store('audio');
        $issue->original_audio = basename($path);
        $issue->save();

        \Log::debug('Starting chain of issue #'. $issue->id);
        TransformIssueAudioToFlac::withChain([
            new TranscriptAudioJob($issue),
            new CreateIssueJob($issue),
            new AttachIssueAudioJob($issue)
        ])->dispatch($issue);

        return ['status' => true, 'file' => $path];
    }
}
