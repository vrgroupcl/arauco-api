<?php

namespace App\Http\Middleware;

use App\User;
use Closure;
use Firebase\JWT\JWT;
use Lcobucci\JWT\Parser;

class ValidateJWTToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $token = $this->getTokenFromRequest($request);
        $key = $this->getPublicKey();

        try {
            $token = JWT::decode((string) $token, $key, $this->getValidAlgorithm()); // Parses from a string
        } catch (\Exception $e) {
            return response()->json(['error' => 'INVALID_TOKEN', 'error_message' => $e->getMessage()], 401);
        }
        //@TODO: Remove this
        if (!isset($token->email)) {
            $token->email = 'vsagredo@vrgroup.cl';
        }
        if (!isset($token->group)) {
            $token->group = '-';
        }
        if (!isset($token->email)) {
            return response()->json(['error' => 'TOKEN_WITHOUT_EMAIL'], 401);
        }

        $this->loginAraucoUser([
            'name' => $token->unique_name,
            'location' => $token->group,
            'email' => $token->email,
        ]);

        return $next($request);
    }

    protected function getTokenFromRequest(\Illuminate\Http\Request $request)
    {
        return $request->get('arauco_token', $this->getTokenFromHeader($request));
    }
    protected function getTokenFromHeader(\Illuminate\Http\Request $request)
    {
        return str_replace('Bearer ', '', $request->header('Authorization'));
    }
    /**
     * @return string
     */
    protected function getPublicKey(): string
    {
        $key = "-----BEGIN CERTIFICATE-----\n";
        $key .= config('adfs.public_key');
        $key .= "\n-----END CERTIFICATE-----";

        return $key;
    }
    /**
     * @return array
     */
    protected function getValidAlgorithm(): array
    {
        return [config('adfs.jwt_algo')];
    }
    protected function loginAraucoUser($userData)
    {
        if (!$userData['email']) {
            return false;
        }

        \Log::debug('Getting user email: ' . $userData['email']);

        $user = User::updateOrCreate(['email' => $userData['email']], [
            'name' => $userData['name'],
            'location' => $userData['location'],
        ]);

        \Log::debug('User id: ' . $user->id);

        auth()->loginUsingId($user->id);
    }
}
