<?php

namespace App\Nova;

use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\Boolean;
use Laravel\Nova\Fields\DateTime;
use Laravel\Nova\Fields\File;
use Laravel\Nova\Fields\ID;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\Number;
use Laravel\Nova\Fields\Select;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Textarea;
use Laravel\Nova\Http\Requests\NovaRequest;

class Issue extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = 'App\Issue';

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'id';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id', ''
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {

        return [
            ID::make()->sortable(),
            BelongsTo::make('User'),
            Text::make('external_id')->sortable()->hideFromIndex(),
            Text::make('ticket_id')->sortable(),
            Select::make('Category')->sortable()->options([
                'sap', 'network', 'software', 'other'
            ]),
            Text::make('Status'),
            Textarea::make('Message')->alwaysShow(),
            File::make('Original Audio')->disk('audios'),
            File::make('Transformed Audio')->disk('audios'),
            Textarea::make('Transcripted Text')->alwaysShow(),
            Number::make('Transcripted Confidence')->resolveUsing(function ($confidence) {
                return $confidence ? number_format($confidence * 100, 1, ',', '.') . '%' : '-';
            }),
            Boolean::make('Saw Failed'),
            Boolean::make('Attachment Sent'),
            Boolean::make('Ready to Sync'),
            Boolean::make('Has Audio'),
            Textarea::make('Saw Solution'),
            DateTime::make('Created At')->hideFromIndex(),
            DateTime::make('Updated At')->hideFromIndex(),
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}
