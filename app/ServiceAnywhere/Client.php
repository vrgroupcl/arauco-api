<?php namespace App\ServiceAnywhere;

use GuzzleHttp\Cookie\CookieJar;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Middleware;
use Illuminate\Support\Facades\Cache;

class Client
{
    public $http;
    private $tenantId;
    protected $logger;
    public $container = [];

    private static $instance;

    /**
     * Client constructor.
     * @param null $httpClient
     */
    private function __construct($httpClient = null)
    {
        $this->tenantId = config('serviceanywhere.tenant_id');

        if ($httpClient) {
            $this->http = $httpClient;
        } else {
            $this->http = new \GuzzleHttp\Client([
                'base_uri' => config('serviceanywhere.base_uri'),
                'handler' => $this->createLoggingHandlerStack([
                    'REQUEST: {method} {uri} HTTP/{version} {req_body}',
                    'RESPONSE: {code} - {res_body}',
                ]),
            ]);
        }
    }

    protected function createLoggingHandlerStack(array $messageFormats)
    {
        $stack = \GuzzleHttp\HandlerStack::create();

        collect($messageFormats)->each(function ($messageFormat) use ($stack) {
            // We'll use unshift instead of push, to add the middleware to the bottom of the stack, not the top
            $stack->unshift(
                $this->createGuzzleLoggingMiddleware($messageFormat)
            );
        });

        return $stack;
    }

    protected function getLogger()
    {
        if (! $this->logger) {
            $this->logger = with(new \Monolog\Logger('api-consumer'))->pushHandler(
                new \Monolog\Handler\RotatingFileHandler(storage_path('logs/api-consumer.log'))
            );
        }

        return $this->logger;
    }

    protected function createGuzzleLoggingMiddleware(string $messageFormat)
    {
        return \GuzzleHttp\Middleware::log(
            $this->getLogger(),
            new \GuzzleHttp\MessageFormatter($messageFormat)
        );
    }

    /**
     * @param null $httpClient
     * @return Client
     */
    public static function getInstance($httpClient = null): self
    {
        if (!static::$instance) {
            static::$instance = new static($httpClient);
        }

        return static::$instance;
    }
    
    public function loginUsingConfig()
    {
        $this->login(config('serviceanywhere.auth.username'), config('serviceanywhere.auth.password'));
    }

    /**
     * @param string $username
     * @param string $password
     */
    public function login(string $username, string $password)
    {
        $response = $this->http->post('/auth/authentication-endpoint/authenticate/login', [
            'json' => [
                'Login' => $username,
                'Password' => $password
            ]
        ]);


        $token = (string) $response->getBody();

        $jar = CookieJar::fromArray([
            'LWSSO_COOKIE_KEY' => $token
        ], config('serviceanywhere.cookie_domain'));


        $baseUri = config('serviceanywhere.base_uri') . '/rest/' . $this->tenantId . '/';


        $this->http = new \GuzzleHttp\Client([
            'handler' => $this->createLoggingHandlerStack([
                '{method} {uri} HTTP/{version} {req_headers} {req_body} ',
                'RESPONSE: {code} - {res_body}',
            ]),
            'base_uri' => $baseUri,
            'cookies' => $jar,
        ]);
    }

    /**
     * @param null|string $filter
     * @return Request
     */
    public function getIncidents($filter = null): Request
    {
        $fields = [
            'Id',
            'Status',
            'Solution',
            'OwnedByPerson',
            'OwnedByPerson.Id',
            'OwnedByPerson.Name',
            'OwnedByPerson.Email',

            'RequestedByPerson',
            'RequestedByPerson. Id',
            'RequestedByPerson.Name',
            'RequestedByPerson.Email'
        ];

        $params = [
            'query' => [
                'layout' => implode(',', $fields),
                'TENANTID' => $this->tenantId
            ]
        ];

        if ($filter) {
            $params['query'] = array_merge($params['query'], ['filter' => $filter]);
        }

        return (new Request($this->http))->setRequestType('get')->setUri('ems/Incident/')->setParams($params);
    }

    /**
     * @param string $ownerId
     * @return Request
     */
    public function getRequestsByOwnerId(string $ownerId): Request
    {
        return $this->getIncidents('OwnedByPerson.Id = \'' . $ownerId . '\'');
    }

    /**
     * @param string $id
     * @return Request
     */
    public function getRequestById(string $id): Request
    {
        $fields = [
            'Id',
            'Status',
            'Solution',
            'OwnedByPerson',
            'OwnedByPerson.Id',
            'OwnedByPerson.Name',
            'OwnedByPerson.Email',

            'RequestedByPerson',
            'RequestedByPerson.Id',
            'RequestedByPerson.Name',
            'RequestedByPerson.Email'
        ];

        $params = [
            'query' => [
                'layout' => implode(',', $fields),
                'TENANTID' => $this->tenantId
            ]
        ];

        return (new Request($this->http))->setRequestType('get')->setUri('ems/Request/' . $id)->setParams($params);
    }

    /**
     * @param string $requesterId
     * @return Request
     */
    public function getRequestsByRequesterId(string $requesterId): Request
    {
        return $this->getIncidents('RequestedByPerson.Id = \'' . $requesterId . '\'');
    }

    /**
     * @param $id
     * @return Request
     */
    public function getPersonById($id): Request
    {
        $params = [
            'query' => [
                'layout' => 'Id,Name,Email',
                'TENANTID' => $this->tenantId
            ]
        ];

        return (new Request($this->http))->setRequestType('get')->setUri('ems/Person/' . $id)->setParams($params);
    }

    /**
     * @param $id
     * @return Request
     */
    public function getPersonByEmail($email): Request
    {
        $params = [
            'query' => [
                'layout' => 'Id,Name,Email',
                'filter' => "PhaseId='New' and Email = '" . strtolower($email) . "'",
//                'TENANTID' => $this->tenantId
            ],
        ];

        return (new Request($this->http))->setRequestType('get')->setUri('ems/Person/')
            ->setParams($params);
    }

    /**
     * @param $category
     * @param $email
     * @param $description
     * @return Request
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function createIncident($category, $email, $description): Request
    {
        $offering = $this->mapOffering($category);
        $requester = $this->getRequesterIdByEmail($email);
//        if (!$requestedById = config('serviceanywhere.requester_id')) {
//            $requestedById = strval($requester);
//        }
        $data = [
            "operation" => "CREATE",
            'entities' => [
                [
                    'entity_type' => 'Request',
                    'properties' => [
                        "RequestsOffering" => $offering,
                        "CreationSource" => "CreationSourceMobile",
                        "ImpactScope" => "SingleUser",
                        "RequestedByPerson" => strval($requester),
                        "RequestedForPerson" => strval($requester),
                        "Active" => true,
                        "Urgency" => "SlightDisruption",
                        // "RegisteredForActualService" => "238836",
                        //"CompletionCode" => "CompletionCodeIncidentResolved",
                        "DisplayLabel" => "Incidente de App Arauco TI",
                        "Priority" => "HighPriority",
                        "Description" => $description
                    ]
                ]
            ]
        ];

        \Log::info(print_r($data, true));

        return (new Request($this->http))
            ->setRequestType('POST')
            ->addHeader('User-Agent', 'Apache-HttpClient/7.1')
            ->setUri('ems/bulk')
            ->setParams(['json' => $data]);
    }

    protected function mapOffering($category)
    {
        $categories = config('serviceanywhere.categories');

        return $categories[$category] ?? null;
    }

    /**
     * @param $email
     * @return int
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getRequesterIdByEmail($email)
    {
        try {
            if ($result = $this->getPersonByEmail($email)->execute()) {
                $result = $result->entities;
                if ($result && $result[0]) {
                    return (int)$result[0]->properties->Id;
                }
            }
        } catch (\Exception $e) {
            \Log::error('Failed to get person ' . $email);
        }

        // Hardcore vsagredo
        return config('serviceanywhere.requester_id');
    }

    /**
     * @param $filePath
     * @return Request
     */
    public function addAttachment($filePath)
    {
        return (new Request($this->http))
            ->setRequestType('POST')
            ->addHeader('User-Agent', 'Apache-HttpClient/7.1')
            ->setUri('ces/attachment')
            ->setParams([
                'multipart' => [
                    [
                        'Content-Disposition' => 'form-data',
                        ['Content-Type' => 'audio/flac'],
                        'name' => 'files[]',
                        'contents' => fopen($filePath, 'r')
                    ]
                ]
            ]);
    }

    /**
     * @param $incidentId
     * @param array $properties
     * @return Request
     */
    public function updateIncident($incidentId, $properties = [])
    {
        $properties = $properties + ['Id' => $incidentId];

        $data = [
            "operation" => "UPDATE",
            'entities' => [
                [
                    'entity_type' => 'Request',
                    'properties' => $properties
                ]
            ]
        ];

        return (new Request($this->http))
            ->setRequestType('POST')
            ->addHeader('User-Agent', 'Apache-HttpClient/7.1')
            ->setUri('ems/bulk')
            ->setParams(['json' => $data]);
    }
}
