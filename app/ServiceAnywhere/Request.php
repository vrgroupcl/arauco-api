<?php namespace App\ServiceAnywhere;

use GuzzleHttp\TransferStats;

class Request
{
    protected $uri;
    protected $params;
    protected $http;
    protected $requestType;
    protected $responseUrl;
    protected $headers = [];


    /**
     * Request constructor.
     * @param $http
     */
    public function __construct(\GuzzleHttp\Client $http)
    {
        $this->http = $http;
    }

    /**
     * @param bool $rawReturn
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function execute($rawReturn = false)
    {
        $response = $this->http->request($this->getRequestType(), $this->getUri(), $this->getParams())
                ->getBody();


        if ($rawReturn) {
            return $response;
        }

        return json_decode((string)$response);
    }

    /**
     * @param int $number
     * @return $this
     */
    public function skip($number = 0): Request
    {
        $this->params['query'] = array_merge($this->params['query'], ['skip' => $number]);
        return $this;
    }

    /**
     * @param int $number
     * @return $this
     */
    public function take($number = 0): Request
    {
        $this->params['query'] = array_merge($this->params['query'], ['size' => $number]);
        return $this;
    }

    /**
     * @return array
     */
    public function getParams(): array
    {
        return array_merge($this->params, [
            'headers' => $this->getHeaders(),
            'on_stats' => function (TransferStats $stats) {
                $this->setResponseUrl($stats->getEffectiveUri());
            }
        ]);
    }

    /**
     * @param array $params
     * @return Request
     */
    public function setParams($params): Request
    {
        $this->params = $params;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getHeaders()
    {
        return $this->headers;
    }

    /**
     * @param mixed $headers
     * @return Request
     */
    public function setHeaders($headers): Request
    {
        $this->headers = $headers;
        return $this;
    }

    /**
     * @param $name
     * @param $value
     * @return Request
     */
    public function addHeader($name, $value): Request
    {
        $this->headers[$name] = $value;

        return $this;
    }

    /**
     * @return string
     */
    public function getUri(): string
    {
        return $this->uri;
    }

    /**
     * @param string $uri
     * @return Request
     */
    public function setUri($uri): Request
    {
        $this->uri = $uri;
        return $this;
    }

    /**
     * @return string
     */
    public function getRequestType(): string
    {
        return $this->requestType;
    }

    /**
     * @param string $requestType
     * @return Request
     */
    public function setRequestType($requestType): Request
    {
        $this->requestType = $requestType;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getResponseUrl()
    {
        return $this->responseUrl;
    }

    /**
     * @param mixed $responseUrl
     */
    public function setResponseUrl($responseUrl): void
    {
        $this->responseUrl = $responseUrl;
    }
}
