<?php

function validation_response($msg, $field = 'general', $statusCode = 422)
{
    throw new \Illuminate\Validation\ValidationException(null, response()->json([
        'errors' => [
            $field => [$msg]
        ]
    ], $statusCode));
}
