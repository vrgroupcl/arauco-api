<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class DeployCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'deploy';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Deploy Update of the app';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        chdir(base_path());
        $output = '';
        // execute command
        exec("git pull origin develop", $output);
        // print output from command
        $this->comment( implode( PHP_EOL, $output ) );
        \Log::debug(print_r($output, true));

        exec("composer install --prefer-dist --no-dev --no-interaction --optimize-autoloader", $output);
        // print output from command
        $this->comment( implode( PHP_EOL, $output ) );
        \Log::debug(print_r($output, true));

        exec("php artisan horizon:terminate", $output);
        // print output from command
        $this->comment( implode( PHP_EOL, $output ) );
        \Log::debug(print_r($output, true));

        $this->info('Deploy completed! 🚀');
    }
}
