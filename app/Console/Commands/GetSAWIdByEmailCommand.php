<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class GetSAWIdByEmailCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'saw_get_user_id {email}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get user id of servyce anywhere account based on their email address';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $client = \App\ServiceAnywhere\Client::getInstance();
        $client->loginUsingConfig();
        $defaultUserId = config('serviceanywhere.requester_id');
        $email = $this->argument('email');
        $userId = $client->getRequesterIdByEmail($email);
        if ($userId == $defaultUserId) {
            $this->error('The user email "'  . $email . '" was not found');
            return;
        }
        $this->info('User found with ID #' . $userId);
    }
}
