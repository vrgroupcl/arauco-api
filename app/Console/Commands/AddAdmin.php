<?php

namespace App\Console\Commands;

use App\User;
use Illuminate\Console\Command;

class AddAdmin extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'add_admin';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Add Admin User';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $user = new User();
        $user->name = $this->ask('Nombre');
        $user->email = $this->ask('Email');
        $user->password = bcrypt($this->ask('Clave'));
        $user->is_admin = true;
        $user->save();

        $this->info('Usuario creado satisfactoriamente con ID #' . $user->id);
    }
}
