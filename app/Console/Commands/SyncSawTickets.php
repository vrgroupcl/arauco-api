<?php

namespace App\Console\Commands;

use App\Issue;
use App\Jobs\SyncStatusFromServiceAnywhere;
use Illuminate\Console\Command;

class SyncSawTickets extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync:saw';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sync SAW tickets';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Issue::where('status', Issue::STATUS_OPEN)
            ->orWhere('status', Issue::STATUS_WAITING_REQUESTER_RESPONSE)
            ->where('created_at', '>', today()->subMonths(1))
            ->orderBy('id', 'DESC')
            ->get()
            ->each(function (Issue $issue) {
                SyncStatusFromServiceAnywhere::dispatch($issue);
            });
    }
}
