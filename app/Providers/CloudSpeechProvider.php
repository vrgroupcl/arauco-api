<?php

namespace App\Providers;

use Google\Cloud\Speech\SpeechClient;
use Illuminate\Support\ServiceProvider;

class CloudSpeechProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(SpeechClient::class, function () {
            return new SpeechClient([
               'projectId' => config('services.google_speech.project_id'),
               'languageCode' => config('services.google_speech.language_code'),
               'keyFilePath' => config('services.google_speech.key_file')
            ]);
        });
    }
}
