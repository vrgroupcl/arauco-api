<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Issue extends Model
{
    const STATUS_PROCESSING = 'processing';
    const STATUS_PROCESSING_AUDIO = 'processing_audio';
    const STATUS_PROCESSING_TRANSCRIPTION = 'processing_transcription';
    const STATUS_ADDING_TO_SAW = 'creating_in_saw';
    const STATUS_ATTACHING_AUDIO_SAW = 'attaching_audio_to_saw';
    const STATUS_OPEN = 'open';
    const STATUS_WAITING_REQUESTER_RESPONSE = 'waiting_response';
    const STATUS_CLOSED = 'closed';
    const STATUS_FAILED_TO_SYNC_SAW = 'failed_to_sync_saw';
    const STATUS_FAILED_TO_TRANSCRIPT = 'failed_to_transcript';
    const STATUS_FAILED_TO_CONVERT_AUDIO = 'failed_to_convert_audio';
    const STATUS_FAILED_TO_UPLOAD_AUDIO = 'failed_to_upload_audio';

    protected $fillable = ['attachment_sent'];

    public $dates = ['created_at', 'updated_at', 'accepted_at', 'synced_at', 'rejected_at', 'closed_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /*
    |--------------------------------------------------------------------------
    | Custom methods
    |--------------------------------------------------------------------------
    */

    public function setStatus($status)
    {
        $this->status = $status;
        $this->save();
    }

    public function hasAudio()
    {
        return $this->original_audio !== null;
    }

    public function scopeNotIncomplete($query)
    {
        return $query->where(function($query) {
            $query->where(function($query) {
                $query->where('has_audio', true)->whereNotNull('original_audio');
            })->orWhere('has_audio', false);
        });
    }
}
