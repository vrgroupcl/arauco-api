<?php

namespace App\Jobs;

use App\Issue;
use Google\Cloud\Speech\SpeechClient;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class TranscriptAudioJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    /**
     * @var Issue
     */
    protected $issue;

    /**
     * The number of times the job may be attempted.
     *
     * @var int
     */

    public $timeout = 25;

    /**
     * Determine the time at which the job should timeout.
     *
     * @return \DateTime
     */
    public function retryUntil()
    {
        return now()->addSeconds(30);
    }

    /**
     * Create a new job instance.
     *
     * @param Issue $issue
     */
    public function __construct(Issue $issue)
    {
        //
        $this->issue = $issue;
    }

    /**
     * Execute the job.
     *
     * @return void
     * @throws \Exception
     */
    public function handle()
    {
        \Log::debug('Transcripting audio file of issue #'. $this->issue->id);
        if ($this->issue->transcripted_text != '') {
            //Already transcripted
            \Log::debug('Already transcripted. Issue #'. $this->issue->id);
            return;
        }

        if ($this->issue->transformed_audio == null) {
            //It has not a compatible audio to transcript yet
            throw new \Exception('Issue has not a compatible audio to transcript yet');
        }

        $this->issue->setStatus(Issue::STATUS_PROCESSING_TRANSCRIPTION);

        /** @var SpeechClient $speechClient */
        $speechClient = app()->make(SpeechClient::class);
        $audioPath = storage_path('app/audio/'. $this->issue->transformed_audio);
        \Log::debug('Going to send audio to google #'. $this->issue->id);
        $results = $speechClient->recognize(fopen($audioPath, 'r'));
        \Log::debug('Audio sended correctly #'. $this->issue->id . '. Response: ' . print_r($results, true));
        
        if (!isset($results[0])) {
            \Log::error('Failed transcripting audio with google #' . $this->issue->id);
            \Log::error(print_r($results, true));
            $this->issue->setStatus(Issue::STATUS_FAILED_TO_TRANSCRIPT);
            throw new \Exception('Failed transcripting audio with google #' . $this->issue->id);
            return;
        }
        $this->issue->transcripted_text = $results[0]->topAlternative()['transcript'];
        $this->issue->transcripted_confidence = $results[0]->topAlternative()['confidence'];
        $this->issue->ready_to_sync = true;
        $this->issue->status = Issue::STATUS_ADDING_TO_SAW;
        $this->issue->save();

        \Log::debug('Transcription ready for issue #'. $this->issue->id);
    }

    public function failed()
    {
        \Log::error('Failed transcripting audio with google');
        $this->issue->setStatus(Issue::STATUS_FAILED_TO_TRANSCRIPT);
    }
}
