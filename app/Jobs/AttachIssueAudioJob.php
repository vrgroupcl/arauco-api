<?php

namespace App\Jobs;

use App\Issue;
use App\ServiceAnywhere\Client;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class AttachIssueAudioJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    /**
     * @var Issue
     */
    protected $issue;

    /**
     * Create a new job instance.
     *
     * @param Issue $issue
     */
    public function __construct(Issue $issue)
    {
        $this->issue = $issue;
    }

    /**
     * Execute the job.
     *
     * @return void
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function handle()
    {
        if (!$this->issue->ticket_id) {
            throw new \Exception('The issue has no been created on SAW before. It has not a ticket id');
        }
        \Log::debug('Attaching audio file of issue #'. $this->issue->id);
        $this->issue->setStatus(Issue::STATUS_ATTACHING_AUDIO_SAW);
        $client = Client::getInstance();
        $client->loginUsingConfig();

        $audioPath = storage_path('app/audio/' . $this->issue->transformed_audio);
        $uploadAttachmentRequest = $client->addAttachment($audioPath);

        //try {
            $attachment = $uploadAttachmentRequest->execute();

            \Log::debug('Attachment uploaded successfully ' . $attachment->guid);

            $data = [
                "complexTypeProperties" => [
                    [
                        "properties" => [
                            "id" => $attachment->guid,
                            "file_name" => $attachment->name,
                            "size" => $attachment->contentLength,
                            "mime_type" => $attachment->contentType
                        ]
                    ]
                ]
            ];

            $result = $client->updateIncident($this->issue->ticket_id, [
                'RequestAttachments' => json_encode($data)
            ])
                ->execute();

        if ($result->entity_result_list[0]->completion_status === 'OK') {
            \Log::debug('Request linked to attachment ' . $attachment->guid);
            $this->issue->attachment_sent = true;
            $this->issue->status = Issue::STATUS_OPEN;
            $this->issue->save();
        } else {
            \Log::error('Failed to upload attachment...');
            \Log::error(print_r($result, true));
            $this->issue->setStatus(Issue::STATUS_FAILED_TO_UPLOAD_AUDIO);
        }
//        } catch (\Throwable $e) {
//            \Log::error('Failed to upload attachment');
//            \Log::error($e->getMessage());
//        }
    }

    public function failed(\Exception $e)
    {
        \Log::error('Failed to upload attachment');
        \Log::error($e->getMessage());
        $this->issue->setStatus(Issue::STATUS_FAILED_TO_UPLOAD_AUDIO);
    }
}
