<?php

namespace App\Jobs;

use App\Issue;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use mysql_xdevapi\Exception;

class TransformIssueAudioToFlac implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    /**
     * @var Issue
     */
    private $issue;

    /**
     * Create a new job instance.
     *
     * @param Issue $issue
     */
    public function __construct(Issue $issue)
    {
        //
        $this->issue = $issue;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        \Log::debug('Transforming audio file of issue #'. $this->issue->id);
        if ($this->issue->transformed_audio != '') {
            //Already transformed
            \Log::debug('Issue already transformed: '. $this->issue->id);
            return true;
        }
        $this->issue->setStatus(Issue::STATUS_PROCESSING_AUDIO);

        $parts = pathinfo($this->issue->original_audio);
        $basename = $parts['basename'];
        $filename = $parts['filename'];
        $extension = $parts['extension'] ?? 'wav';
        $newBasename = $filename . '.flac';

        chdir(storage_path('app/audio'));
        $return_code = null;
        $output = null;
        if ($extension === 'wav') { //iOS audio
            exec("ffmpeg -i {$basename} -af aformat=s16:44100 -ac 1 {$newBasename} -y ", $output, $return_code);
//        } elseif ($extension === 'aac') { //Android
        } else {
            exec("ffmpeg -i {$basename} -af aformat=s16:44100 -ac 1 {$newBasename} -y", $output, $return_code);
        }
        \Log::info('ffmpeg return code: ' . $return_code);
        if ($return_code !== 0) {
            throw new \Exception('Failed to execute ffmpeg command');
        }

        $this->issue->transformed_audio = $newBasename;
        $this->issue->status = Issue::STATUS_PROCESSING_TRANSCRIPTION;
        $this->issue->save();
    }

    public function failed()
    {
        \Log::error('Failed transforming audio to flac');
        $this->issue->status = Issue::STATUS_FAILED_TO_CONVERT_AUDIO;
        $this->issue->save();
    }
}
