<?php

namespace App\Jobs;

use App\Issue;
use App\ServiceAnywhere\Client;
use Google\Cloud\Speech\SpeechClient;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Bus\Queueable;
use Illuminate\Http\UploadedFile;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

/**
 * Class CreateIssueJob
 * @package App\Jobs
 */
class CreateIssueJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    /**
     * @var Issue
     */
    protected $issue;

    /**
     * Create a new job instance.
     *
     * @param $issue
     */
    public function __construct(Issue $issue)
    {
        $this->issue = $issue;
    }

    /**
     * Execute the job.
     *
     * @return void
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function handle()
    {
        \Log::debug('Creating issue #'. $this->issue->id);
        if ($this->issue->ticket_id) {
            \Log::debug('Issue #'. $this->issue->id . ' already had a ticket id');
            return;
        }
        if (!$this->issue->ready_to_sync) { // || $this->issue->saw_failed ???
            //\Log::debug('Skipping ticket ' . $this->issue->id);
            throw new \Exception('Issue is not ready to sync');
        }

        $this->issue->setStatus(Issue::STATUS_ADDING_TO_SAW);

        $client = Client::getInstance();
        $client->loginUsingConfig();
        $description = $this->getRequestDescription();

        try {
            $response = $client->createIncident(
                $this->issue->category,
                $this->issue->user->email,
                $description
            )->execute();
        } catch (ClientException $exception) {
            \Log::error($exception->getMessage());
            \Log::error('#' . $this->issue->id . ' Failed creating ticket on SAW. Response: ' . $exception->getResponse()->getBody());
            if ($exception->getResponse()->getStatusCode() === 401) {
                // Sleep for 5 seconds and try again
                \Log::debug('Wating for 5 seconds...');
                sleep(5);
            }
            throw $exception;

        }


        if ($response->entity_result_list[0]->completion_status !== "OK") {
            \Log::error('Failed to create ticket on service anywhere...');
            \Log::error(print_r($response, true));
            throw new \Exception('Issue has not been created on SAW');
        }
        $this->issue->ticket_id = $response->entity_result_list[0]->entity->properties->Id;
        if ($this->issue->hasAudio()) {
            $this->issue->status = Issue::STATUS_ATTACHING_AUDIO_SAW;
        } else {
            $this->issue->status = Issue::STATUS_OPEN;
        }

        $this->issue->save();
        \Log::debug('Issue #'. $this->issue->id . ' created successfully with SAW ID #' . $this->issue->ticket_id);
        return;
    }

    /**
     *
     */
    public function failed()
    {
        $this->issue->saw_failed = true;
        $this->issue->status = Issue::STATUS_FAILED_TO_SYNC_SAW;
        $this->issue->save();
    }
    /**
     * @return string
     */
    protected function getRequestDescription(): string
    {
        $description = '';
        if ($this->issue->message) {
            $description .= "<p>Mensaje escrito por el usuario: " . $this->issue->message ?? '-' . "</p>";
            $description .= "<p></p>";
        }
        $confidence = $this->issue->transcripted_confidence ? round(
            $this->issue->transcripted_confidence * 100,
            1
        ) : null;
        if ($confidence) {
            $description .= "<p>Confianza en la transcripción: " . $confidence . "%</p>";
        }
        if ($this->issue->transcripted_text) {
            $description .= "<p>Transcripción del audio: " . $this->issue->transcripted_text ?? '-' . "</p>";
        }

        $name = $this->issue->user->name;
        $email = $this->issue->user->email;
        $description .= "<p></p>";
        $description .= '<h3>Datos del solicitante</h3>';
        $description .= "<span style='width: 150px; display:inline-block;'>Nombre: </span><strong>$name</strong><br/>";
        $description .= "<span style='width: 150px; display:inline-block;'>Email: </span><strong>$email</strong>";

        return $description;
    }
}
