<?php

namespace App\Jobs;

use App\Issue;
use App\ServiceAnywhere\Client;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class SyncStatusFromServiceAnywhere
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    /**
     * @var Issue
     */
    protected $issue;

    /**
     * Create a new job instance.
     *
     * @param Issue $issue
     */
    public function __construct(Issue $issue)
    {
        //
        $this->issue = $issue;
    }

    /**
     * Execute the job.
     *
     * @return void
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function handle()
    {
        if (!$this->issue->ticket_id) {
            return;
        }

        $client = Client::getInstance();
        $client->loginUsingConfig();
        $request = $client->getRequestById($this->issue->ticket_id)->execute();
        if (!count($request->entities) == 1) {
            return;
        }
        $request = $request->entities[0];
        //\Log::debug('Request sync: ' . print_r($request, true));
        if ($request->properties->Status === 'RequestStatusPendingVendor') {
            $this->issue->status = Issue::STATUS_WAITING_REQUESTER_RESPONSE;
            $this->issue->saw_solution = $request->properties->Solution ?? null;
            $this->issue->save();
        }

        if ($request->properties->Status === 'RequestStatusInProgress') {
            $this->issue->status = Issue::STATUS_OPEN;
            $this->issue->save();
        }

        if ($request->properties->Status === 'RequestStatusComplete') {
            $this->issue->status = Issue::STATUS_CLOSED;
            $this->issue->saw_solution = $request->properties->Solution ?? null;
            $this->issue->closed_at = now();
            $this->issue->save();
        }
    }
}
