<?php
/**
 * Created by Eduardo Aguad <eduaguad@gmail.com>
 * Date: 10/20/18
 * Time: 16:10
 */

return [
    'base_uri' => env('SERVICEANYWHERE_BASE_URI'),
    'tenant_id' => env('SERVICEANYWHERE_TENANT_ID'),
    'cookie_domain' => env('SERVICEANYWHERE_COOKIE_DOMAIN'),
    'cache_expiration_time' => env('SERVICEANYWHERE_TOKEN_EXPIRATION_TIME', 60), // seconds
    'auth' => [
        'username' => env('SERVICEANYWHERE_USERNAME'),
        'password' => env('SERVICEANYWHERE_PASSWORD')
    ],
    'categories' => [
        'sap' => env('SERVICEANYWHERE_SAP', 238836),
        'software' => env('SERVICEANYWHERE_SOFTWARE', 238138),
        'network' => env('SERVICEANYWHERE_NETWORK', 238831),
        'other' => env('SERVICEANYWHERE_OTHER', 238537)
    ],
    'requester_id' => env('SERVICEANYWHERE_REQUESTER_ID', 226248)
];
