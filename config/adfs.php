<?php

return [
    'url' => env('ADFS_URL', 'https://adfs.arauco.cl'),
    'client_id' => env('ADFS_CLIENT_ID'),
    'redirect_uri' => env('ADFS_REDIRECT_URI'),
    'public_key' => env('ADFS_PUBLIC_KEY'),
    'jwt_algo' => env('ADFS_JWT_ALGORITHM'),
    'is_proxy_adfs' => env('PROXY_ADFS', false),
    'proxy_redirect_url' => env('PROXY_REDIRECT_URL', "https://apimda.arauco.cl/oauth")
];
