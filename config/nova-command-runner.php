<?php

return [
    'commands' => [
        'Route cache'      => ['run' => 'route:cache', 'type' => 'info', 'group' => 'Cache'],
        'Config cache'     => ['run' => 'config:cache', 'type' => 'info', 'group' => 'Cache'],
        'View cache'       => ['run' => 'view:cache', 'type' => 'info', 'group' => 'Cache'],

        'Route clear'     => ['run' => 'route:clear', 'type' => 'warning', 'group' => 'Clear Cache'],
        'Config clear'    => ['run' => 'config:clear', 'type' => 'warning', 'group' => 'Clear Cache'],
        'View clear'      => ['run' => 'view:clear', 'type' => 'warning', 'group' => 'Clear Cache'],
        'Cache Clear'      => ['run' => 'cache:clear', 'type' => 'warning', 'group' => 'Clear Cache'],

        'Deploy!'   => ['run' => 'deploy', 'type' => 'success', 'group' => 'Deploy'],
    ],
    'history'  => 10,
];
