FROM nginx:1.15.8-alpine

COPY docker/vhost.conf /etc/nginx/conf.d/default.conf
RUN mkdir -p /var/www/html/public
COPY ./public/ /var/www/html/public/

