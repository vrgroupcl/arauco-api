FROM php:7.3-fpm-alpine as builder
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/bin --filename=composer
COPY . /var/www/html
WORKDIR /var/www/html
RUN composer install --prefer-dist --no-dev
RUN cp .env.example .env & php artisan key:generate


FROM php:7.3-fpm-alpine
WORKDIR /var/www/html
COPY --from=builder /var/www/html .
RUN chown -R www-data:www-data /var/www
